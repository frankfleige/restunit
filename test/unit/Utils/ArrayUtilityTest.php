<?php

namespace Frankfleige\Restunit\Test\Unit\Utils;

use Frankfleige\Restunit\Utils\ArrayUtility;
use PHPUnit\Framework\TestCase;

/**
 * Class ArrayUtilityTest
 * @package Frankfleige\Restunit\Test\Unit\Utils
 */
class ArrayUtilityTest extends TestCase
{

    /**
     * @param $needles
     * @param $haystack
     * @param $expected
     * @dataProvider dataProviderEquals
     */
    public function testEquals($needles, $haystack, $expected)
    {
        $this::assertSame(
            $expected,
            ArrayUtility::equals($needles, $haystack)
        );
    }

    /**
     * @param $needles
     * @param $haystack
     * @param $expected
     * @dataProvider dataProviderHasValues
     */
    public function testHasValues($needles, $haystack, $expected)
    {
        $this::assertSame(
            $expected,
            ArrayUtility::hasValues($needles, $haystack)
        );
    }

    /**
     * @return array[]
     */
    public function dataProviderEquals(): array
    {
        return [
            'all empty' => [[], [], true],
            'empty needle' => [[], ['foo'], false],
            'empty haystack' => [['foo'], [], false],
            'exact match' => [['foo', 'bar'], ['bar', 'foo'], true],
            'missing within needle' => [['bar'], ['bar', 'foo'], false],
            'missing within haystack' => [['foo', 'bar'], ['bar'], false]
        ];
    }

    /**
     * @return array[]
     */
    public function dataProviderHasValues(): array
    {
        return [
            'all empty' => [[], [], true],
            'empty needle' => [[], ['foo'], false],
            'empty haystack' => [['foo'], [], false],
            'exact match' => [['foo', 'bar'], ['bar', 'foo'], true],
            'missing within needle' => [['bar'], ['bar', 'foo'], true],
            'missing within haystack' => [['foo', 'bar'], ['bar'], false]
        ];
    }
}
