<?php
/** @noinspection PhpUnused */


namespace Frankfleige\Restunit\Framework;

/**
 * Class HttpTestCase
 * @package Frankfleige\Restunit\Framework
 */
abstract class HttpTestCase extends HttpAssertion
{
}
