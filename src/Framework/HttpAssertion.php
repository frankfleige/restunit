<?php
/** @noinspection PhpUnused */


namespace Frankfleige\Restunit\Framework;

use Frankfleige\Restunit\Utils\ArrayUtility;
use Frankfleige\Restunit\Utils\ContentType;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

/**
 * Class HttpAssertion
 * @package Frankfleige\Restunit\Framework
 */
abstract class HttpAssertion extends TestCase
{
    /**
     * Asserts that the response has the specified status code.
     * @param int $expectedStatusCode expected status code
     * @param ResponseInterface $response response
     */
    public static function assertResponseHasStatusCode(
        int $expectedStatusCode,
        ResponseInterface $response
    ) {
        static::assertSame($expectedStatusCode, $response->getStatusCode());
    }

    /**
     * Asserts that the response is of type "application/json".
     * @param ResponseInterface $response response
     */
    public static function assertIsJSONResponse(ResponseInterface $response)
    {
        static::assertIsContentType(
            ContentType::APPLICATION_JSON,
            $response
        );
    }

    /**
     * Asserts that the response has the given content type.
     * @param string $expectedContentType expected value of header "Content-Type"
     * @param ResponseInterface $response response
     */
    public static function assertIsContentType(
        string $expectedContentType,
        ResponseInterface $response
    ) {
        static::assertSame(
            $expectedContentType,
            $response->getHeaderLine('Content-Type')
        );
    }

    /**
     * Asserts that the response is of type "text/plain".
     * @param ResponseInterface $response response
     */
    public static function assertIsTextResponse(ResponseInterface $response)
    {
        static::assertIsContentType(
            ContentType::TEXT_PLAIN,
            $response
        );
    }

    /**
     * Asserts that the response has a header with the given name.
     *
     * @param string $headerName header name
     * @param ResponseInterface $response response
     */
    public static function assertResponseHasHeader(
        string $headerName,
        ResponseInterface $response
    ) {
        static::assertTrue($response->hasHeader($headerName));
    }

    /**
     * Asserts that the response has a header with the given name and the
     * given values.
     * @param string $headerName header name
     * @param array $values array with values the header should have
     * @param ResponseInterface $response response
     * @param bool $exactMatch If <code>true</code> (default), the header must
     * only contain the given values. Otherwise the header must contain at least
     * the given values.
     */
    public static function assertResponseHasHeaderWithValues(
        string $headerName,
        array $values,
        ResponseInterface $response,
        bool $exactMatch = true
    ) {
        $headerValues = $response->getHeader($headerName);
        static::assertCount(count($values), $headerValues);
        $match = ($exactMatch) ?
            ArrayUtility::equals($values, $headerValues) :
            ArrayUtility::hasValues($values, $headerValues);
        static::assertTrue($match);
    }

    /**
     * Asserts that the given response has an empty body.
     * @param ResponseInterface $response
     */
    public static function assertHasEmptyBody(ResponseInterface $response)
    {
        static::assertEmpty($response->getBody()->getContents());
    }

    /**
     * Asserts that the given response has not an empty body.
     * @param ResponseInterface $response
     */
    public static function assertHasNonEmptyBody(ResponseInterface $response)
    {
        static::assertNotEmpty($response->getBody()->getContents());
    }
}
