<?php


namespace Frankfleige\Restunit\Utils;

/**
 * Class ArrayUtility
 * @package Frankfleige\Restunit\Utils
 */
abstract class ArrayUtility
{
    /**
     * Checks if given values are part of the given haystack.
     * @param array $needles
     * @param array $haystack
     * @return bool
     */
    public static function hasValues(array $needles, array $haystack): bool
    {
        if (empty($needles) && !empty($haystack)) {
            return false;
        }
        return static::equals(
            array_intersect($needles, $haystack),
            $needles
        );
    }

    /**
     * Checks if two arrays have equal values.
     * @param array $needles needles
     * @param array $haystack haystack
     * @return bool Will return <code>true</code> if all needles are part of the
     * given haystack. Otherwise <code>false</code> will be returned.
     * If the needles are empty, but the haystack is not, <code>false</code>
     * will be returned.
     */
    public static function equals(array $needles, array $haystack): bool
    {
        if (count($needles) !== count($haystack)) {
            return false;
        }
        return count(array_intersect($needles, $haystack)) === count($needles);
    }
}
