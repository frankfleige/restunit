<?php


namespace Frankfleige\Restunit\Utils;

/**
 * Class ContentType
 * @package Frankfleige\Restunit\Utils
 */
abstract class ContentType
{
    /**
     * application/json content type
     */
    public const APPLICATION_JSON = "application/json";

    /**
     * text/plain content type
     */
    public const TEXT_PLAIN = "text/plain";
}
